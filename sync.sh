#!/bin/bash

VEHICLE_HOST=icarus.local

function quitit {
    echo 'Doe als volgt: "./sync.sh"'
    exit 0
}

USERNAME=pi
PATH_ON_SERVER=/home/$USERNAME/icarus


function syncThatShit {

    echo
    echo Syncing to $VEHICLE_HOST
    echo

    # Sync everything except the ground/ folder to the SU
    rsync --delete --exclude-from 'sync_exclude.txt' -azvv -e ssh '.' $USERNAME@$VEHICLE_HOST:$PATH_ON_SERVER
}

syncThatShit

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    inotifywait -r -m -e close_write --format '%w%f' '.' | while read MODFILE
    do
        syncThatShit
    done
elif [[ "$OSTYPE" == "darwin"* ]]; then
    fswatch -o -r '.' -e 'synced_at.*' -e 'run.py' -e 'config.py' | while read MODFILE
    do
        syncThatShit
    done
else
    echo "Only Linux and MacOS are supported. Snorrie!!";
fi
