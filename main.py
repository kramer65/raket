#!/usr/bin/env python3

import board
import busio
import adafruit_lps2x  # pip install adafruit-circuitpython-lps2x
from datetime import datetime
import gpiozero
import RPi.GPIO as GPIO
from time import sleep

# stuff for the buzzer
GPIO.setwarnings(False)
buzzer_gpio=23
GPIO.setup(buzzer_gpio, GPIO.OUT)
buzzer = GPIO.PWM(buzzer_gpio, 100)
GPIO.output(buzzer_gpio, True)
buzzer.start(80)
e4 = 329
buzzer.ChangeFrequency(e4)

# pressure sensor
i2c = busio.I2C(board.SCL, board.SDA)
lps = adafruit_lps2x.LPS25(i2c)

# Init the parachute deployment
parachute = gpiozero.LED(17)

# init a log
f = open('pressures.log', 'a')

recent_pressures = []
current_pressure = 0
pressure_hist = []  # contains one current_pressure per second
parachute_deployed = False


while True:
    recent_pressures.append(lps.pressure)
    if len(recent_pressures) > 20:
        recent_pressures = recent_pressures[-20:]

    current_pressure = sum(recent_pressures) / len(recent_pressures)

    if len(pressure_hist) == 0 or (datetime.now() - pressure_hist[-1][0]).seconds > 1:
        pressure_hist.append((datetime.now(), current_pressure))
        f.write(str(current_pressure) + "\n")
        print(current_pressure)

    pressure_hist = pressure_hist[-20:]
    if not parachute_deployed and len(pressure_hist) > 5:
        if current_pressure > pressure_hist[-3][1]+0.2:  # is the current_pressure more than 0.3 higher than the perssure of 3 seconds ago
            print("KABOOM PARACHUTE!!!!!!!!!!!!!!!!")
            parachute.on()
            print("AFTER ZE KABOOM")
            parachute_deployed = True


# this never runs.. :-(
f.close()
buzzer.stop()
GPIO.cleanup()
