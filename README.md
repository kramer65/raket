# Raket

Awesome Rocket Thrust Vectoring Controls

## Run it locally

    docker-compose up

## New Raspbian for in the rocket
Follow the instructions in the whisky project


## Todo's

- ~~Read out the imu
- ~~Be able to control the servo's using the PWM board
- ~~Make the X and Y servo's respond to a change in the imu
- ~~Write the state to a log file
- Prepare the SD-card with the OS and everything on it
- Test it in the rocket
- Introduce PIDs for the X and the Y
- Speed up the loop and test it again
- Hardware needed:
    - a battery for in the rocket
    - A buck down/up converter
- Add a way to deploy the parachute
