package main

// Inspired by https://github.com/sergiorb/pca9685-golang/blob/master/examples/servo.go
import (
	"github.com/op/go-logging"
	"github.com/sergiorb/pca9685-golang/device"
	log "github.com/sirupsen/logrus"
	"golang.org/x/exp/io/i2c"
)

const (
	I2C_ADDR		= "/dev/i2c-1"
	PCA9685_ADDR	= 0x40
	MIN_PULSE		= 150
	MAX_PULSE		= 650
	X_CHANNEL		= 0
	Y_CHANNEL		= 1
)

func setPercentage(p *device.Pwm, percent float32) {
	pulseLength := int((MAX_PULSE - MIN_PULSE) * percent / 100 + MIN_PULSE)

	if !PRODUCTION {
		//log.Error("### Not running on production, so we're NOT setting the PWM for this device")
		return
	}

	err := p.SetPulse(0, pulseLength)
	if err != nil {
		log.Error("Failed setting the PWM pulse for")
	}
}

type Controls struct {
	x			*device.Pwm
	y			*device.Pwm
}
func (self *Controls) Init() int {
	if !PRODUCTION {
		// If we're not in production we stop it here, because on any device not
		// having i2c ports, the code below will end in an error
		log.Info("NOT RUNNING ON THE BOAT, SO NOT INITIALIZING THE GPIO PINS")
		return 0
	}

	logger := logging.Logger{}
	dev, err := i2c.Open(&i2c.Devfs{Dev: I2C_ADDR}, PCA9685_ADDR)
	if err != nil {
		log.Fatal(err)
	}

	pca := device.NewPCA9685(dev, "Servo Controller", MIN_PULSE, MAX_PULSE, &logger)
	pca.Frequency = 50.0
	pca.Init()

	self.x = pca.NewPwm(X_CHANNEL)
	self.y = pca.NewPwm(Y_CHANNEL)

	return 0  // Success! :D
}
func (self *Controls) X(directionPoints float64) {
	setPercentage(self.x, float32(directionPoints))
}
func (self *Controls) Y(directionPoints float64) {
	setPercentage(self.y, float32(directionPoints))
}
func NewControls() Controls {
	controls := Controls{}
	controls.Init()
	return controls
}
