package main

import (
	"bufio"
	log "github.com/sirupsen/logrus"
	"io"
	"math"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Orientation struct {
	sync.RWMutex
	Yaw        int
	Pitch      int
	Roll       int
	Altitude   int
}
func (self *Orientation) Store(yaw, pitch, roll, altitude int) {
	self.Lock()
	defer self.Unlock()
	self.Yaw = yaw
	self.Pitch = pitch
	self.Roll = roll
	self.Altitude = altitude
}
func (self *Orientation) Get() (yaw, pitch, roll, altitude int) {
	self.Lock()
	defer self.Unlock()
	return self.Yaw, self.Pitch, self.Roll, self.Altitude
}
func GetOrientations(r io.Reader, orientation *Orientation) {
	if !PRODUCTION && SIMULATION{
		log.Info("Not in production mode, so this is testing, which means we generate testing values for the IMU")

		yaw, pitch, roll, altitude := 0, -100, -100, 10
		for {
			yaw++
			pitch++
			roll++
			altitude++
			if yaw == 360 {
				yaw = 0
			}
			if pitch == 100 {
				pitch = -100
				roll = -100
				altitude = 0
			}
			// yaw, pitch, roll, altitude = 0, 6, 7, 10
			orientation.Store(yaw, pitch, roll, altitude)
			time.Sleep(1 * time.Second)
		}
	}

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		// log.Debug(line)

		// Loop over the results to remove the empty strings
		imuResults := []string{}
		for _, value := range strings.Split(line, " ") {
			if len(value) > 0 {
				imuResults = append(imuResults, value)
			}
		}
		yawRaw, _ := strconv.ParseFloat(imuResults[0], 64)
		pitchRaw, _ := strconv.ParseFloat(imuResults[1], 64)
		rollRaw, _ := strconv.ParseFloat(imuResults[2], 64)

		yaw_float := 0.0
		pitch_float := 0.0
		roll_float := 0.0
		if pitchRaw > 90 {
			yaw_float = 180 + yawRaw // For a yaw_float between 0 and 360
			// yaw_float = -(180 - yawRaw)  // For a yaw_float between -180 and +180
			pitch_float = 180 - pitchRaw
			if rollRaw > 0 {
				roll_float = (180 - rollRaw) * -1
			} else {
				roll_float = 180 - math.Abs(rollRaw)
			}
		} else if pitchRaw < -90 {
			yaw_float = 180 + yawRaw // For a yaw_float between 0 and 360
			// yaw_float = -(180 - yawRaw)  // For a yaw_float between -180 and +180
			pitch_float = 180 - math.Abs(pitchRaw)
			if rollRaw > 0 {
				roll_float = (180 - rollRaw) * -1
			} else {
				roll_float = 180 - math.Abs(rollRaw)
			}
		} else {
			yaw_float = yawRaw
			pitch_float = pitchRaw
			roll_float = rollRaw
		}
		yaw := int(yaw_float)
		pitch := int(pitch_float)
		roll := int(roll_float)

		// log.Info("CLEAN:  yaw_float", yaw_float, "pitch_float", pitch_float, "roll_float", roll_float)
		orientation.Store(yaw, pitch, roll, 0.0)
	}
}
func (self *Orientation) init() {
	if !PRODUCTION {
		// Not in production mode, so this is testing, which means we generate testing values for the imu
		stdout, _ := exec.Command("ls").StdoutPipe() // A useless command just to get an io.Reader to supply to the GetOrientations
		go GetOrientations(stdout, self)
		return
	}

	cmd := exec.Command("minimu9-ahrs", "--output", "euler", "--i2c-bus=/dev/i2c-1")
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		log.Error("IMU cmd.StdoutPipe() ERROR: ", err)
	}
	err = cmd.Start()
	if err != nil {
		log.Error("IMU cmd.Start() ERROR: ", err)
	}
	go GetOrientations(stdout, self)
	go cmd.Wait()
}
func NewOrientation() *Orientation {
	orientation := &Orientation{}
	orientation.init()
	return orientation
}
