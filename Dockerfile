FROM debian:buster

# Install packages
RUN apt update && apt upgrade -y && apt install -y \
    sudo \
    git \
    golang \
    go-dep \
    gpsd

# Set GOPATH
RUN echo "export GOPATH=$HOME" >> ~/.bash_profile
ARG GOPATH=/root
ENV GOPATH ${GOPATH}
RUN echo $GOPATH

# Ports
EXPOSE 6000-6100

# Copy the dependencies
#COPY Gopkg.toml /root/src/whisky/
#COPY Gopkg.lock /root/src/whisky/
#COPY vendor/ /root/src/whisky/vendor/

# Put code in container
COPY . /root/src/raket

WORKDIR /root/src/raket

# Get dependencies
RUN go get

# Compile the code
RUN go build -o bin *.go

# Run the binary
CMD /root/src/raket/bin
