package main

import (
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"time"
)

const LOG_FILE_NAME = "state.log"
var PRODUCTION = false
var SIMULATION = true


type State struct {
	// All numbers except Altitude and Roll are numbers between -100 and 100
	X int			`json:"x"`
	Y int			`json:"y"`
	Roll int		`json:"roll"`		// between 0 and 360
	XServo int		`json:"x_servo"`
	YServo int		`json:"y_servo"`
	Altitude int	`json:"altitude"`	// TODO: No idea how this works yet. It probably needs to be calibrated on startup
}


func main() {
	fmt.Println("Starting up that sjiiiiiiit")

	// Open a log file to which I write all the settingsData every loop
	stateLogfile, err := os.OpenFile(LOG_FILE_NAME, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0777)
	if err != nil {
		log.Fatal(err)
	}
	defer stateLogfile.Close()

	// Init IMU
	orientation := NewOrientation()

	// Init Thrust Vector Controls
	controls := NewControls()

	// Init settingsdata object
	state := State{}

	for {
		state.Roll, state.X, state.Y, state.Altitude = orientation.Get()  // get compass, ignore rest of IMU output
		
		// Decide on how to steer
		if state.X < -10 {
			state.XServo = 50
		} else if state.X > 10 {
			state.XServo = -50
		}
		if state.Y < -10 {
			state.YServo = 50
		} else if state.Y > 10 {
			state.YServo = -50
		}
		
		// Do the steering
		controls.X(float64(state.XServo))
		controls.Y(float64(state.YServo))

		// Print out some values
		if time.Now().Second() % 10 == 0 {
			fmt.Println("x  xControl       y    yControl")
		}
		fmt.Println(state.X, "   ", state.XServo, "              ", state.Y, " ", state.YServo)

		// Write to a log file
		stateJSON, err := json.Marshal(state)
		if err != nil {
			log.Error("Error marshalling the state data. Error: ", err)
		}
		stateJSON = []byte(string(stateJSON)+"\n")  // add a newline
		if _, err := stateLogfile.Write(stateJSON); err != nil {
			log.Error(err)
		}

		// Slow that shit down
		time.Sleep(1 * time.Second)  // TODO: reduce sleep to 50 milliseconds
	}
}